<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Listar Producto</title>
</head>
<body>
<h1>LISTAR PRODUCTO</h1>
<table>
	<tr>
		<th align="left">Id</tdh>
		<th align="left">Nombre</tdh>
		<th align="left">Cantidad</tdh>
		<th align="left">Precio</tdh>
		<th align="left">Fecha Creacion</tdh>
		<th align="left">Fecha Actualización</tdh>
		<th align="left">Accion</tdh>
	</tr>
	<c:forEach var="producto" items="${listaProductos}">
		<tr>
			<td><a href="Producto?opcion=actualizar&id=<c:out value="${producto.id_prod}"></c:out>"><c:out value="${producto.id_prod}"></c:out></a></td>
			<td><c:out value="${producto.nombre}"></c:out></td>
			<td><c:out value="${producto.cantidad}"></c:out></td>
			<td><c:out value="${producto.precio}"></c:out></td>
			<td><c:out value="${producto.fecha_creacion}"></c:out></td>
			<td><c:out value="${producto.fecha_actualizacion}"></c:out></td>
			<td><a href="Producto?opcion=eliminar&id2=<c:out value="${producto.id_prod}"></c:out>">Eliminar</a></td>
		</tr>
	</c:forEach>
</table>

</body>
</html>