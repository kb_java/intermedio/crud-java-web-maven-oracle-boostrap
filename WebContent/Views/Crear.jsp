<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Crear Producto</title>
</head>
<body>
<h1>CREAR PRODUCTO</h1>
<form action="Producto" method="post">
<input type="hidden" name="opcion" value="guardar">
<table border="1">
	<tr>
		<td>Nombre</td>
		<td><input type="text" name="txtNombre" value=""></td>
	</tr>
	<tr>
		<td>Cantidad</td>
		<td><input type="text" name="txtCantidad" value=""></td>
	</tr>
	<tr>
		<td>Precio</td>
		<td><input type="text" name="txtPrecio" value=""></td>
	</tr>
	<tr>		
		<td colspan="2" align="right"><input type="submit" name="btnEnviar" value="Enviar">  |  <input type="reset" name="btnCancelar" value="Cancelar"></td>
	</tr>
</table>

</form>

</body>
</html>