<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Editar Producto</title>
</head>
<body>
<h1>EDITAR PRODUCTO</h1>
<form action="Producto" method="post">
<c:set var="producto" value="${detalleProducto}"></c:set>

<input type="hidden" name="opcion" value="editar">
<input type="hidden" name="id" value="${producto.id_prod}">
	<table border="1">
		<tr>
			<td>Nombre</td>
			<td><input type="text" name="txtNombre" value="${producto.nombre}"></td>
		</tr>
		<tr>
			<td>Cantidad</td>
			<td><input type="text" name="txtCantidad" value="${producto.cantidad}"></td>
		</tr>
		<tr>
			<td>Precio</td>
			<td><input type="text" name="txtPrecio" value="${producto.precio}"></td>
		</tr>
		<tr>		
			<td colspan="2" align="right"><input type="submit" name="btnActualizar" value="Actualizar">  |  <input type="reset" name="btnCancelar" value="Cancelar"></td>
		</tr>
	</table>
</form>
</body>
</html>