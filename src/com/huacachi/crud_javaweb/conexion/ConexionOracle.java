package com.huacachi.crud_javaweb.conexion;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;


public class ConexionOracle {
	
	private static BasicDataSource bds = null;
	
	private static DataSource dataSource() {
		if(bds == null) {
			bds = new BasicDataSource();
			bds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
			bds.setUsername("ehuacachi");
			bds.setPassword("9xPiwZ3r");
			bds.setUrl("jdbc:oracle:thin:@localhost:1521:xe");			
			bds.setInitialSize(20);
			bds.setMaxIdle(15);
			bds.setMaxTotal(20);
			bds.setMaxWaitMillis(5000);	
		}			
		return bds;
	}
	
		
	public static Connection getConnection() throws SQLException {
		return dataSource().getConnection();
	}

}



