package com.huacachi.crud_javaweb.conexion;

import java.sql.Connection;

public class main {

	public static void main(String[] args) {
		
		try(Connection cn = ConexionOracle.getConnection()) {
			if (cn != null) {
				System.out.println("Conexion exitosa");
			}else {
				System.out.println("Error en la conexion");
			}		
						
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
