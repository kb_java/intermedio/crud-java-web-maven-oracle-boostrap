package com.huacachi.crud_javaweb.modelo;

import java.sql.Date;

public class Producto {
	private int id_prod;
	private String nombre;
	private double cantidad;
	private double precio;
	private Date fecha_creacion;
	private Date fecha_actualizacion;
	
	public Producto(int id_prod, String nombre, double cantidad, double precio, Date fecha_creacion,
			Date fecha_actualizacion) {
		super();
		this.id_prod = id_prod;
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.precio = precio;
		this.fecha_creacion = fecha_creacion;
		this.fecha_actualizacion = fecha_actualizacion;		
		
	}
	
	public Producto() {
		// TODO Auto-generated constructor stub
	}

	public int getId_prod() {
		return id_prod;
	}

	public void setId_prod(int id_prod) {
		this.id_prod = id_prod;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public Date getFecha_actualizacion() {
		return fecha_actualizacion;
	}

	public void setFecha_actualizacion(Date fecha_actualizacion) {
		this.fecha_actualizacion = fecha_actualizacion;
	}

	@Override
	public String toString() {
		//Nos sirven para ver el valor del Objero
		return "Producto [id_prod=" + id_prod + ", name=" + nombre + ", cantiad=" + cantidad + ", precio=" + precio
				+ ", fecha_creacion=" + fecha_creacion + ", fecha_actualizacion=" + fecha_actualizacion + "]";
	}
	
	
	
	

	

	
	
	

}
