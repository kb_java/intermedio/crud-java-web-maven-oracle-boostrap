package com.huacachi.crud_javaweb.controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.huacachi.crud_javaweb.DAO.ProductoDAO;
import com.huacachi.crud_javaweb.modelo.Producto;

/**
 * Servlet implementation class ProductoController
 */
@WebServlet(description = "Administra peticiones de la tabla Producto", urlPatterns = { "/Producto" })
public class ProductoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductoController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String opcion = request.getParameter("opcion");
		
		switch (opcion) {
		case "crear":
			RequestDispatcher requestDispatcher= request.getRequestDispatcher("./Views/Crear.jsp");
			requestDispatcher.forward(request, response);			
			break;
		case "listar":
			List<Producto> lista = new ArrayList<Producto>();
			lista = ProductoDAO.obtenerProductos();
			
			//corrroboramos el Listamos de productos
			for (Producto producto : lista) {
				System.out.println(producto);
			}
			
			request.setAttribute("listaProductos", lista);
			RequestDispatcher requestDispatcher2= request.getRequestDispatcher("./Views/Listar.jsp");
			requestDispatcher2.forward(request, response);
			break;
		case "actualizar":
			
			int id = Integer.parseInt(request.getParameter("id"));
			System.out.println(id);
			
			Producto producto = new Producto();
			producto = ProductoDAO.obtenerProducto(id);
			System.out.println(producto);
			
			request.setAttribute("detalleProducto", producto);
			RequestDispatcher requestDispatcher3 = request.getRequestDispatcher("./Views/Editar.jsp");
			requestDispatcher3.forward(request, response);
			
		case "eliminar":
			int id2 = Integer.parseInt(request.getParameter("id2"));
			ProductoDAO.elimianarProducto(id2);
			RequestDispatcher requestDispatcher4 = request.getRequestDispatcher("Index.jsp");
			requestDispatcher4.forward(request, response);
		

		default:
			break;
		}
				
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		String opcion = request.getParameter("opcion");
		Producto producto = new Producto();
		Date fechaActual = new Date();
				
		switch (opcion) {
		case "guardar":
			producto.setNombre(request.getParameter("txtNombre"));
			producto.setCantidad(Double.parseDouble(request.getParameter("txtCantidad")));
			producto.setPrecio(Double.parseDouble(request.getParameter("txtPrecio")));
			producto.setFecha_creacion(new java.sql.Date(fechaActual.getTime()));
			
			ProductoDAO.crearProducto(producto);
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("Index.jsp");
			requestDispatcher.forward(request, response);
			break;
			
		case "editar":
			System.out.println("Ingres� a editar");
			System.out.println(request.getParameter("id"));
			
			producto.setId_prod(Integer.parseInt(request.getParameter("id")));
			producto.setNombre(request.getParameter("txtNombre"));
			producto.setCantidad(Double.parseDouble(request.getParameter("txtCantidad")));
			producto.setPrecio(Double.parseDouble(request.getParameter("txtPrecio")));
			producto.setFecha_actualizacion(new java.sql.Date(fechaActual.getTime()));
			
			ProductoDAO.actualizarProducto(producto);
			RequestDispatcher requestDispatcher2 = request.getRequestDispatcher("Index.jsp");
			requestDispatcher2.forward(request, response);
			
		default:
			break;
		}
				
				
	}
}
